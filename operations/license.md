# CoreMedia Monitoring Toolbox - Operations Guide

## License

Alle CoreMedia Contentserver (CMS, MLS, RLS) werden mit seperaten Lizenzen ausgestattet.

Diese beinhalten eine Gracetime Period.

Sobald diese einsetzt, werden entsprechende Ausgaben ins Logfile geschrieben.

Nach Ablauf der Gracetime beenden sich die

----

Ist die Lizenz abgelaufen, dann schalten sich die Services in eine Art Standby-Modus.
Sollte das passieren, sind sämtliche Komponenten Out-of-Business. Im Frontend werden dadurch keine Seiten mehr ausgeliefert!

Lizenzdateien können via Email über den Support bei Coremedia (support@coremedia.com) bestellt werden.

Bei einem Austausch von Lizenzen sollten die Services nicht neu gestartet werden müssen.

----

## Logfile

Im Logfile der Contentserver können weitere Informationen besorgt werden, wenn man diese nicht in einem Monitoring hat.

### Speicherort der Lizenzdatei

```bash
grep "cap.server.license" $logfile
```

# Gültigkeit der Lizenz

Sollten die CoreMedia Tools installiert worden sein, kann man auch hierüber die Lizendaten ausgeben:

```bash
/opt/coremedia/content-management-server-tools/bin/cm license -u admin
/opt/coremedia/master-live-server-tools/bin/cm license -u admin
/opt/coremedia/replication-live-server-tools/bin/cm license -u admin
```



----

## Operating

| Fehler  | ToDo      |
| :------ | :-------- |
| **Der Lizenzcheck alarmiert**  | Eine neue Lizenz muss über ein Supportticket angefordert werden! |
